{
    "id": "90ac254b-1c17-4ec2-8b21-c332df283219",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fontDevbig",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Source Sans Pro",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2e5277a6-053b-4f68-885c-6720d8cf58d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "868cc12a-df92-420e-8469-3fcb76d5e54d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 116,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "13e03705-b3ff-4c2a-890b-0ff3ff9f6b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 109,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cff3d7c3-0259-45d5-927e-14ae742a07f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 99,
                "y": 46
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0269a5c2-a6ee-4803-b978-ed1d17c46845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 90,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b7a1d1b2-2ca9-436f-b044-96fd1e40396a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 75,
                "y": 46
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9832ac1e-ccb1-4bc7-bb91-64d122fdd48e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 63,
                "y": 46
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "72edf694-36e2-454d-8300-80b121e4cdda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 59,
                "y": 46
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "46565bc1-cd0f-4e5b-a0bb-16f2dc6ff0bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 53,
                "y": 46
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2fffa64d-6755-48b2-a083-1d34022ee901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 47,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b7b7dde9-0d65-400a-95bc-c61d380c17a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 121,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "20f683a9-0cd6-40da-b9f7-5f8795a9cd91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 37,
                "y": 46
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b0c9458f-9e4b-431a-bfbe-6ec5cd30d3f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 21,
                "y": 46
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "335e867c-b12d-4b04-a07c-f3bd501cd214",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 14,
                "y": 46
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b68bd1b5-e829-42bd-b65f-958c54f14d25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 10,
                "y": 46
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "02d49599-9c02-4d12-a556-d12cde4b4093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "4c061c87-e71f-4810-9dc9-2122fc60dd4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 240,
                "y": 24
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "03b15489-3a6e-4259-893b-4de446a2258a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 231,
                "y": 24
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a4c1b879-0160-4136-8694-2e55927f67bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 221,
                "y": 24
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6c82d1ca-2cb1-4227-b3ff-b4d28d303017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 211,
                "y": 24
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "44f50d24-1654-48eb-b47e-0d00035c0b3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 201,
                "y": 24
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f0c7ca3f-e8e1-419b-a738-12813e2d2a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 27,
                "y": 46
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "109ba1ff-6639-435e-ae79-bbdbeab86997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 129,
                "y": 46
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c5996e96-c760-4b50-90cf-0f9d4e795a90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 139,
                "y": 46
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "33eabcfb-365b-4fa0-9ae8-238fe0489fc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 149,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1bc79606-963e-42ee-b714-1234a0e7dd5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 98,
                "y": 68
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0579b48f-3cf1-4750-a461-b21afa822266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 94,
                "y": 68
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "78af2eaf-d0d6-4cb0-b626-a52006c92cbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 88,
                "y": 68
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2fd2bc33-df2b-4c39-a430-77dab7d5c816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 78,
                "y": 68
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3914db81-596f-4137-a465-8580b5d57404",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 68,
                "y": 68
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3df0d86b-b755-4e97-9f39-ac489954e74e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 58,
                "y": 68
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "62a7bf7d-5e27-4e04-ba24-1ac3562b1e89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 49,
                "y": 68
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "64ea7306-2979-424c-9655-9061873f6880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 34,
                "y": 68
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b76e9df1-09d2-487f-844b-bbb1f4904c59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 23,
                "y": 68
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "460f697a-fda6-4e75-a79c-f199e6efde90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 13,
                "y": 68
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "02cf044f-9ead-4586-bc59-cc4d083d3e6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "38fea7e4-1c49-4233-ac19-3ec5cac785b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 243,
                "y": 46
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "362d86e6-b717-479a-93d6-25ffecab38aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 234,
                "y": 46
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ab128b1d-a6da-4f38-b47b-1b1c5f928be9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 225,
                "y": 46
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4844d31c-ad9d-4b58-8ea8-4be628cd7bad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 214,
                "y": 46
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1db59a8b-7441-48cf-9d0d-f274ceab917c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 204,
                "y": 46
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c9589bce-3891-4ea1-8b18-888364d95929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 200,
                "y": 46
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2214de2e-c064-48d2-be9b-4041a7ae0fbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 191,
                "y": 46
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a8e080c6-701a-4a15-a477-c040b9fc2c63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 180,
                "y": 46
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d8f53e1e-0e08-4cb2-b210-1545cb64cc65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 171,
                "y": 46
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "89386959-c6e9-422b-a509-5d2f500d62b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 159,
                "y": 46
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "37165a1d-d624-4c07-a4b0-746d4b4ce64b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 191,
                "y": 24
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d5050493-b0d2-4aac-8ae0-e6d0ea4e1efe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 179,
                "y": 24
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e4557d37-51c8-442f-83d8-de83f078d1b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 169,
                "y": 24
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "1138f87e-58a7-44b2-abc3-b262d2f335f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a28c2e05-6891-4101-a021-ba0c4e63e25f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "240bf0f0-a9cf-47c6-864d-b62fd98dd644",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5ccae98f-4d6f-44c5-9486-3b5b5a480c9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "202f85cd-a8be-400f-9e1f-da3f28720d16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c22dc900-82ed-45dc-bde9-220cc5a7f4c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "381476cc-8a58-4f7b-891d-3a6a312f5ae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fa6ebe80-07dd-418f-badc-73b53f7094eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "20a985e7-08db-494b-9578-6a4a1e0eb0f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "193184ec-f25e-49c2-9058-6ff330cf165f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "950b4eaa-4c94-43be-a284-2ee858f1fa00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b8353be9-2b00-41fe-aa0b-5493d9570a39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5c06df36-abde-455c-9424-f34ba2180243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fdf37905-ed10-4642-a204-576d935f4c24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "92e6146f-73cc-4c8e-9483-b7cc593ada4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3429c7fb-d3bc-4146-a800-ceed24ed7146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d91fc482-b507-4154-9627-2859b8948700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6880b94b-52a8-46a6-a9af-df8ce42b7698",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4ecbde0a-8feb-4e89-aef7-37ebbca41869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "6f3f66f8-996d-40b9-a899-7db12945e036",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "00b648ce-d395-4ed5-91e9-dce223ce1a01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e1c4656f-4844-4cc4-805b-c81e3e1a9ca3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6d7bbc82-e7aa-4051-a12e-9076a941a5f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e40c8427-b19d-424b-a8da-82f19af5c5c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 63,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "063f3257-b424-4f84-9f32-996cddef436d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "33b6fff0-38b3-4289-8433-5c789c0811c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 154,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "4f448a49-ed43-4f84-abbe-e31a97ea7d4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 145,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5eee60cf-9659-4445-a61e-7c5ba18f616f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 140,
                "y": 24
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "10e57369-500a-407b-b917-0c38a06f48ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 126,
                "y": 24
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3d3242e4-1489-4403-8aa6-604e7fd7b210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 117,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "bd11fd3f-7db1-4c27-b600-c6ceb0a33f5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 107,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4c05ea3b-ddfb-4cb1-be3d-7935c2ece549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 97,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6443eefa-5a7d-441f-aef0-5249a37621e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 87,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9d5f1f71-ebdf-44ce-a66b-6d2b566dfc90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 80,
                "y": 24
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c1901ba9-5739-4430-bd44-b982bc797368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 160,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c7f00d98-b7a3-45aa-8366-3aa13f1c7e26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 72,
                "y": 24
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4ac38daa-3f4f-4141-a36b-48b3da53dda5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 54,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "682792ac-52f0-4b9b-92a3-c884dafc3c08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 44,
                "y": 24
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "071d9199-41b7-44dd-8063-42498d6d8eaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 30,
                "y": 24
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "eff09a19-fd64-4820-b0c7-7de653e872ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 21,
                "y": 24
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1e97a502-4d15-4123-bf19-84935a8d36a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 11,
                "y": 24
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4c8c3120-685c-4962-aa2d-de4b913b5414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1a2ed1ea-43a7-4102-815d-503b90ce620e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "20d5de49-1e99-4762-a43d-6eef7f04d5bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "478ede74-326f-4676-a5c4-5dea09a4aa30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0a6f7180-0712-4f64-8aba-a9968a4f8324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 108,
                "y": 68
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4ba4b19b-930e-4275-9352-d2597bfc9ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 20,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 118,
                "y": 68
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}