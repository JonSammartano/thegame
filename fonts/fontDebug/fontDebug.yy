{
    "id": "3a590c2c-74f1-4277-9f75-e2a2099be779",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fontDebug",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "BitPotion",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8dc78000-c75e-45a6-b2c3-a69824feb34c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "548a5ca8-3f91-40ea-b643-e02219ae4b35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 96,
                "y": 59
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "27f863b4-cd45-490b-9bd1-d732b4081709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 90,
                "y": 59
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "dcb2f0bb-1792-4102-a272-5e1ab94b0f3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 79,
                "y": 59
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9cb26bbe-b7e1-43ac-a6e0-ef7aebc7f8a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 59
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "51b7e890-4ab9-4422-a9ef-2bfe5f95af82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 59
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e6ee6fbb-ec7a-4dc1-93bf-f407516b4305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 55,
                "y": 59
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b373177c-20a1-4663-8000-944dbeb90f29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 51,
                "y": 59
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "797bce3e-9388-457b-90bd-7626022916b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 46,
                "y": 59
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0f30b003-2e84-481a-9c16-cfc7d73cb3e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 41,
                "y": 59
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ac555128-27e1-4441-af7f-2f816292da19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 100,
                "y": 59
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "05518c33-5d35-4be6-ba53-cd884939a68c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 33,
                "y": 59
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "10750745-6bf2-4d75-a211-c198fecba8a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 21,
                "y": 59
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "07c16a70-52bf-418b-9739-4bfc36f416dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 14,
                "y": 59
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f319d002-6e52-47a2-b9b8-942ff5a8b133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 10,
                "y": 59
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "985934e3-3436-4997-b497-33f8ec07fe5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6992f63f-c83f-4b07-800b-ff3472c790f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 113,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fb464d01-e622-4209-b980-42caadd34092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 107,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a649ac7d-e01b-4219-bb3b-e9f191ff3fbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ad65d8ee-9bdd-416a-92fa-1db6cc782f09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "95412754-02bc-43d8-9728-116371774617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "00b4989b-d5e4-40ea-8d99-f02a736f5b93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 26,
                "y": 59
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "7fa73422-d416-403a-bf7c-fcd5d72e49da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 108,
                "y": 59
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2677eef3-ce8d-44c2-9249-7bc495da97e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 115,
                "y": 59
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "bbb48958-d088-451a-aa13-8bab9855dc3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bff8a166-968f-4ca1-8bf3-ea7ea3883224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 25,
                "y": 97
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1a8624d1-b775-4d75-8dad-c41f834fbfb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 21,
                "y": 97
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "04396b25-aabc-4fa5-afee-afbfab4a8910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 16,
                "y": 97
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "563009b2-9369-4bee-88d1-c104cc0e5ae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 97
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b8b3644b-74eb-4179-8464-03893db9a087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 97
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d0ba318b-81fa-43b7-ad6f-c67d4f44ae5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 115,
                "y": 78
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "acd7a37a-f08b-45a0-bdce-b8111ec097e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 108,
                "y": 78
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f76bf8c4-b4d2-47c3-b04d-0f470577af26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 98,
                "y": 78
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "712c803c-d243-42d0-bbd6-76eefe09a1c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 91,
                "y": 78
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "72dd6476-530a-4cff-8586-131000b3fbea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 84,
                "y": 78
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1532eecf-753e-4396-923e-fcbbc82f5ad1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 77,
                "y": 78
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7169cbfa-0c3b-4a4b-af96-5d3c023e6ddb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 70,
                "y": 78
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d0d6309e-e1c2-425b-b971-141dfd12fdea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 63,
                "y": 78
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bdafbf1c-efb4-4668-ad84-0d8ac9c1e27c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 56,
                "y": 78
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f2fff81e-f8b4-475c-9a9a-bdd30167dd9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 49,
                "y": 78
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6f9ce29c-d78e-4c95-87a0-799b0fcd9a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 42,
                "y": 78
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5ba217d5-c987-4176-aa37-6716bc42df31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 38,
                "y": 78
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e8ec2341-bad0-473b-9e9d-1043035553d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 31,
                "y": 78
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "69199e35-e1af-4b13-9fd3-39e607fa68fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 24,
                "y": 78
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2e8f9ad9-3ee9-47df-b52d-69717f633a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 17,
                "y": 78
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "acea1acf-3752-449f-81bc-f57e184c9bdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 9,
                "y": 78
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "df0f56b2-cad7-4524-b863-57bc7aaf5096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 40
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0c2e5406-ee01-4ecc-9443-b5a5628f3fe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6516094d-764f-4fef-a40a-75cf9216fb11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 40
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "4911012b-7537-4c3b-be28-0fe2671fa9cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 27,
                "y": 21
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5203c5c1-d3b6-4e95-9415-ea124ff921bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 15,
                "y": 21
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2668211c-ab18-4523-b7e7-e2bc64b6969b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 8,
                "y": 21
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d1768b4a-0b0d-4c2d-8320-0e6d98f98d76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a04e9fbf-638e-416b-8e89-feccc58db36b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b5d150f3-c86b-4ae5-bedf-6e0b6c3380dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c1cd3605-7202-4d69-a02e-a00eed3ee309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6b18ec85-2423-4d47-b89f-0813fe3b29c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b1f5f301-d931-49d9-88c7-564da95c3d84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9a39623a-59ba-4acc-a2f7-b766d56d2e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8321d9d1-3115-4576-bf06-e88eb73f3402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 22,
                "y": 21
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f8efcf19-970b-45af-8ce8-8dea7ec8cd62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "284ff96c-1135-4bd3-9fbb-81b6b8cdacab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f49fc9d3-3df7-452d-8e1f-0f640ee94c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f3a31eb1-251c-448b-9c84-4d5126c19982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b6afbb87-40a7-41ec-ad1c-834379a1538d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b209e480-57f2-4c99-81d4-89444f920c8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "dd22595c-0097-4f12-8742-bb49acefb1a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "db58d5bd-accd-43f0-b0d7-1e022b6cd59f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b2a5f08b-7c05-4ea7-aea0-caba84d462a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "3ea81701-19c0-462c-86a4-3d70922e37ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "fba4e009-30d2-4532-a2e6-dedbe6b931b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "30694d7e-ff67-438a-a516-9183d7cbfc15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 34,
                "y": 21
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4d78cfb4-5864-4640-a7ca-7699f16ff022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 105,
                "y": 21
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "fa61568b-e881-40e9-8c27-1e3a4ae63a1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 41,
                "y": 21
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f32193c4-3195-4f7e-91a9-c9b22e310921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": -2,
                "shift": 2,
                "w": 4,
                "x": 52,
                "y": 40
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b796e4cc-058a-4d7d-b33c-6654f190f27c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 46,
                "y": 40
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5e625db6-9dff-405e-806a-9e3de082acb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 41,
                "y": 40
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "af8672c6-a648-4466-8669-97e94227adef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 31,
                "y": 40
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "190269ab-3713-4b9b-8f1f-ba7e0b6dadc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 23,
                "y": 40
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a30478d0-7b9e-46af-8041-a6247102fbe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 40
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "075ca71c-033a-49f8-a06d-57b04806525b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 40
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4a3ee458-7993-4a56-a32a-e74b00a034f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ff57331d-f917-46b9-8e03-666abe01b091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 119,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "40841b2f-85a3-4cb0-a524-e4c1b35a521a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 40
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d269d6e8-a4c5-4544-9d06-7026b86bd959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 112,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "841625ca-26c3-4890-ac59-e6744aaa07a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 98,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5d1a7c3c-6e23-4ce2-862d-07e362d7c118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 91,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "102ac2bc-473c-4423-b244-87f97cbc5663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 83,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8500129f-815a-4e8e-8112-671b1010d5d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 77,
                "y": 21
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0303e2c0-d71d-44bd-8d83-7c218f474037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 70,
                "y": 21
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f99f83b5-0042-49dc-8bb5-30ced97ee9a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 63,
                "y": 21
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "686f934d-7da9-4c28-93f5-e563ac731534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 56,
                "y": 21
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "5cbb059d-c129-43f0-8df4-bd5529e5640e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 52,
                "y": 21
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d5efc296-9774-4eb6-9632-66aafed12c68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 45,
                "y": 21
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d649a3ca-c972-4d6d-8a8e-c239344e4ed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 32,
                "y": 97
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "6d546682-1838-47e9-951f-21251776b3ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 17,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 42,
                "y": 97
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 14,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}