{
    "id": "cca833b6-7cf7-4224-9702-ca1ebb27616c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDungeonCrawl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3039,
    "bbox_left": 0,
    "bbox_right": 2047,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1c26255-23f4-45a9-ad79-2be9513c8306",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cca833b6-7cf7-4224-9702-ca1ebb27616c",
            "compositeImage": {
                "id": "01f7ffba-befb-4795-94dc-45de133c4cea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1c26255-23f4-45a9-ad79-2be9513c8306",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a72cea9-8b8f-42d2-bd00-99f425eb70fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1c26255-23f4-45a9-ad79-2be9513c8306",
                    "LayerId": "83a7235e-eaa4-4ff8-89c9-5a8a2ec0eab3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3040,
    "layers": [
        {
            "id": "83a7235e-eaa4-4ff8-89c9-5a8a2ec0eab3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cca833b6-7cf7-4224-9702-ca1ebb27616c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2048,
    "xorig": 1024,
    "yorig": 1520
}