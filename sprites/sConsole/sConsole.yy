{
    "id": "622779f1-85f1-4916-88d5-fa39f0349d15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sConsole",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5d71a7b-867d-4e3b-a003-f1d8bc762e2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622779f1-85f1-4916-88d5-fa39f0349d15",
            "compositeImage": {
                "id": "a9df2a8b-58f7-407c-87fc-0582b03ca7d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5d71a7b-867d-4e3b-a003-f1d8bc762e2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1428ee2-7d5a-494d-b031-4655231d95f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5d71a7b-867d-4e3b-a003-f1d8bc762e2d",
                    "LayerId": "14461019-ce85-4e69-820d-58dfbabd7360"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "14461019-ce85-4e69-820d-58dfbabd7360",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "622779f1-85f1-4916-88d5-fa39f0349d15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}