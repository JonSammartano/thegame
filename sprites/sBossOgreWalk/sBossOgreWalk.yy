{
    "id": "baa9e0b3-60d3-4dbf-8db5-392972450645",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBossOgreWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01e28d5d-84da-4a8d-89ea-1fb37226519c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baa9e0b3-60d3-4dbf-8db5-392972450645",
            "compositeImage": {
                "id": "31d1326d-064b-4c31-ab90-288059e51c70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01e28d5d-84da-4a8d-89ea-1fb37226519c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a8e8e1c-d95a-4219-a3c1-ea4c57e0862c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01e28d5d-84da-4a8d-89ea-1fb37226519c",
                    "LayerId": "d0722031-08db-42ce-b15b-3689d6111e7f"
                }
            ]
        },
        {
            "id": "63b2d22d-ca96-4172-a02c-c728fc56747e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baa9e0b3-60d3-4dbf-8db5-392972450645",
            "compositeImage": {
                "id": "3defae99-7292-4fe4-a068-4c8e4dd15dc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63b2d22d-ca96-4172-a02c-c728fc56747e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8355337-b575-463f-b630-a95c7e0d4fe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63b2d22d-ca96-4172-a02c-c728fc56747e",
                    "LayerId": "d0722031-08db-42ce-b15b-3689d6111e7f"
                }
            ]
        },
        {
            "id": "c0f39e34-6478-42c7-bcee-ab532a9a554d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baa9e0b3-60d3-4dbf-8db5-392972450645",
            "compositeImage": {
                "id": "6736e8c8-6bfa-4632-ade9-631c42649e63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0f39e34-6478-42c7-bcee-ab532a9a554d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a669cda-dc5f-4e3a-a30a-78c4ee1270d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0f39e34-6478-42c7-bcee-ab532a9a554d",
                    "LayerId": "d0722031-08db-42ce-b15b-3689d6111e7f"
                }
            ]
        },
        {
            "id": "ef4411ac-809d-41e7-9156-d2449ca5b0a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baa9e0b3-60d3-4dbf-8db5-392972450645",
            "compositeImage": {
                "id": "9043304e-c3b2-4bb5-8367-879d77279c23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef4411ac-809d-41e7-9156-d2449ca5b0a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06f04765-340e-4d3e-b736-b3f30b74aaf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef4411ac-809d-41e7-9156-d2449ca5b0a8",
                    "LayerId": "d0722031-08db-42ce-b15b-3689d6111e7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d0722031-08db-42ce-b15b-3689d6111e7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "baa9e0b3-60d3-4dbf-8db5-392972450645",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}