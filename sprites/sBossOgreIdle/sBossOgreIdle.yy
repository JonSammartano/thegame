{
    "id": "4713e0ee-6d51-484d-887a-a76d5a6774a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBossOgreIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3542bd82-e484-4d1f-beff-f997b3bbf46c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4713e0ee-6d51-484d-887a-a76d5a6774a5",
            "compositeImage": {
                "id": "e2b7a050-b1c0-4c39-8ab6-64bd8b1e3648",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3542bd82-e484-4d1f-beff-f997b3bbf46c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33eaf43a-4ee5-457f-b465-24b3f0b62a42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3542bd82-e484-4d1f-beff-f997b3bbf46c",
                    "LayerId": "6152ef09-4c54-41b2-b361-82f9eb545732"
                }
            ]
        },
        {
            "id": "85a1157f-4409-4fed-a069-76897302d33e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4713e0ee-6d51-484d-887a-a76d5a6774a5",
            "compositeImage": {
                "id": "22e4e8a9-4f06-45bc-9d2f-4b495a8af057",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85a1157f-4409-4fed-a069-76897302d33e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9de004b-ff32-4f63-9f79-012650db0152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85a1157f-4409-4fed-a069-76897302d33e",
                    "LayerId": "6152ef09-4c54-41b2-b361-82f9eb545732"
                }
            ]
        },
        {
            "id": "199d238c-41e7-4815-8760-b1045730c891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4713e0ee-6d51-484d-887a-a76d5a6774a5",
            "compositeImage": {
                "id": "2159d802-f611-4138-a351-b2934ae7edde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "199d238c-41e7-4815-8760-b1045730c891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c339aa64-6d5b-4a04-94ba-f1e9662237ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "199d238c-41e7-4815-8760-b1045730c891",
                    "LayerId": "6152ef09-4c54-41b2-b361-82f9eb545732"
                }
            ]
        },
        {
            "id": "783f3c5b-0036-4f08-80e8-7bf1dbdee813",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4713e0ee-6d51-484d-887a-a76d5a6774a5",
            "compositeImage": {
                "id": "e9a96f2b-740c-4697-85d3-5fcf3c8a5a85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "783f3c5b-0036-4f08-80e8-7bf1dbdee813",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad711a67-f481-4e79-9eb7-7ed25e81cbca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "783f3c5b-0036-4f08-80e8-7bf1dbdee813",
                    "LayerId": "6152ef09-4c54-41b2-b361-82f9eb545732"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6152ef09-4c54-41b2-b361-82f9eb545732",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4713e0ee-6d51-484d-887a-a76d5a6774a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}