{
    "id": "3384e229-85ff-4b05-8bc7-4f245680919a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOrcShamanWalking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3aba147-11d4-4f4d-81a0-499a367c2e08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3384e229-85ff-4b05-8bc7-4f245680919a",
            "compositeImage": {
                "id": "6163ebb9-bc3c-44a0-a9ac-184c3f958d43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3aba147-11d4-4f4d-81a0-499a367c2e08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "871b3ef8-bf96-4b29-842d-76a763300f31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3aba147-11d4-4f4d-81a0-499a367c2e08",
                    "LayerId": "ab2f4015-2100-4094-a0ca-85ef851da958"
                }
            ]
        },
        {
            "id": "d0a2b9dd-f168-4edf-85d0-cc4366420296",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3384e229-85ff-4b05-8bc7-4f245680919a",
            "compositeImage": {
                "id": "d99313d9-3134-4815-9e55-908cf8570bac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0a2b9dd-f168-4edf-85d0-cc4366420296",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c7c55a6-796b-4a9f-8f88-cf12fcecb421",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0a2b9dd-f168-4edf-85d0-cc4366420296",
                    "LayerId": "ab2f4015-2100-4094-a0ca-85ef851da958"
                }
            ]
        },
        {
            "id": "b3ef01ae-e6fc-4c12-908c-946de7fdc0f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3384e229-85ff-4b05-8bc7-4f245680919a",
            "compositeImage": {
                "id": "17306984-e754-40d0-8326-4e237f099efb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3ef01ae-e6fc-4c12-908c-946de7fdc0f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3187686f-1bbf-4701-98bb-d95183292c4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3ef01ae-e6fc-4c12-908c-946de7fdc0f5",
                    "LayerId": "ab2f4015-2100-4094-a0ca-85ef851da958"
                }
            ]
        },
        {
            "id": "48bb3081-0b16-402e-9101-c7b367ff1905",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3384e229-85ff-4b05-8bc7-4f245680919a",
            "compositeImage": {
                "id": "a6da8062-8f95-4b86-829c-21aed47eb115",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48bb3081-0b16-402e-9101-c7b367ff1905",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6372c47f-3a6b-4f33-8193-254cf44b3b38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48bb3081-0b16-402e-9101-c7b367ff1905",
                    "LayerId": "ab2f4015-2100-4094-a0ca-85ef851da958"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "ab2f4015-2100-4094-a0ca-85ef851da958",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3384e229-85ff-4b05-8bc7-4f245680919a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 10
}