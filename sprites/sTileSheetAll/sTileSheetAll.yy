{
    "id": "45afb904-a809-43a0-a0fd-cc56f3569d40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTileSheetAll",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 11,
    "bbox_right": 494,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23a6508e-a683-4136-a063-98325ebd841a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45afb904-a809-43a0-a0fd-cc56f3569d40",
            "compositeImage": {
                "id": "7ca16d84-e0fd-4511-8edb-0cb2c2d316ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23a6508e-a683-4136-a063-98325ebd841a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea0ffcfb-6625-4c0d-8b9b-7f5a61f2ba50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23a6508e-a683-4136-a063-98325ebd841a",
                    "LayerId": "68c76789-963d-4b1d-ac5b-d277462aa6b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "68c76789-963d-4b1d-ac5b-d277462aa6b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45afb904-a809-43a0-a0fd-cc56f3569d40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}