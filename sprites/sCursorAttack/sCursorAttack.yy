{
    "id": "12af5ea6-d010-44dd-9004-59ab333036b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCursorAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0fb9ffb-2c86-4ba7-97cb-bc220c54f2c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12af5ea6-d010-44dd-9004-59ab333036b6",
            "compositeImage": {
                "id": "940d3695-0fcf-429e-b6fb-ffc44dcf4ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0fb9ffb-2c86-4ba7-97cb-bc220c54f2c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "066a1390-fd7e-4e7b-abcb-2a2ed691814e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0fb9ffb-2c86-4ba7-97cb-bc220c54f2c4",
                    "LayerId": "a4e3f2c3-c9cb-4773-817c-f9e93001e36b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "a4e3f2c3-c9cb-4773-817c-f9e93001e36b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12af5ea6-d010-44dd-9004-59ab333036b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 2,
    "yorig": 2
}