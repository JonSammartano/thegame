{
    "id": "35d861fd-3723-4e91-9998-b2fef6cb7ca6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyZombieIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea0ced19-25fd-4bf9-b872-4094935cecae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35d861fd-3723-4e91-9998-b2fef6cb7ca6",
            "compositeImage": {
                "id": "b364995f-95b4-4693-afeb-cd8b044f116e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea0ced19-25fd-4bf9-b872-4094935cecae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44824a59-f474-49f2-bfff-02c1cac87a56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea0ced19-25fd-4bf9-b872-4094935cecae",
                    "LayerId": "c622e9b3-8de9-4791-a3ee-557bc1782b5e"
                }
            ]
        },
        {
            "id": "7679dc66-7fc7-4f7a-9db5-c03aaa35c0fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35d861fd-3723-4e91-9998-b2fef6cb7ca6",
            "compositeImage": {
                "id": "c2e7cc8d-09a2-4af6-bab3-e96392659d22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7679dc66-7fc7-4f7a-9db5-c03aaa35c0fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d3e42da-fd95-4f4c-aad0-4637a3555b5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7679dc66-7fc7-4f7a-9db5-c03aaa35c0fb",
                    "LayerId": "c622e9b3-8de9-4791-a3ee-557bc1782b5e"
                }
            ]
        },
        {
            "id": "b273846e-d498-4f88-91a5-4753e5876d85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35d861fd-3723-4e91-9998-b2fef6cb7ca6",
            "compositeImage": {
                "id": "e8baf205-b8b8-49fe-a18f-34eed571d7ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b273846e-d498-4f88-91a5-4753e5876d85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4f6d8d4-0c46-4461-af01-e9e08da530b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b273846e-d498-4f88-91a5-4753e5876d85",
                    "LayerId": "c622e9b3-8de9-4791-a3ee-557bc1782b5e"
                }
            ]
        },
        {
            "id": "43a348b3-c3f1-4acc-b86f-c6552f7248d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35d861fd-3723-4e91-9998-b2fef6cb7ca6",
            "compositeImage": {
                "id": "49d572f9-24be-4ec7-8e37-870fdfae0476",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a348b3-c3f1-4acc-b86f-c6552f7248d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e43a55f-593e-47be-9153-b4f583937dc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a348b3-c3f1-4acc-b86f-c6552f7248d7",
                    "LayerId": "c622e9b3-8de9-4791-a3ee-557bc1782b5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "c622e9b3-8de9-4791-a3ee-557bc1782b5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35d861fd-3723-4e91-9998-b2fef6cb7ca6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 17
}