{
    "id": "6f6d83b0-37ba-4bdb-87ca-7781fb6515b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCollisionHitbox16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c9c26b4-0623-4053-a96e-666a3f1552f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6d83b0-37ba-4bdb-87ca-7781fb6515b7",
            "compositeImage": {
                "id": "a1a727cf-616b-4bde-98af-e085c6ce5d0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c9c26b4-0623-4053-a96e-666a3f1552f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d63be51-4ad9-43b0-bb7c-7ccc35f44509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c9c26b4-0623-4053-a96e-666a3f1552f4",
                    "LayerId": "bc717936-6602-4720-9bbc-717512a05737"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bc717936-6602-4720-9bbc-717512a05737",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f6d83b0-37ba-4bdb-87ca-7781fb6515b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}