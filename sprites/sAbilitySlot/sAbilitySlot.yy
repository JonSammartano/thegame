{
    "id": "4a775551-f751-4c29-9053-e23d1408b838",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAbilitySlot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e9acaf1-3617-4bbc-be8e-4a0fe2130aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a775551-f751-4c29-9053-e23d1408b838",
            "compositeImage": {
                "id": "26813381-f523-4cc6-9d88-94499b4eefdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e9acaf1-3617-4bbc-be8e-4a0fe2130aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c241a6ff-0b91-4ae1-946c-bf4fb1bcd11e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e9acaf1-3617-4bbc-be8e-4a0fe2130aac",
                    "LayerId": "0b7a4475-e7a1-4f80-bf67-2b3fcfbf7517"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0b7a4475-e7a1-4f80-bf67-2b3fcfbf7517",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a775551-f751-4c29-9053-e23d1408b838",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}