{
    "id": "51fe5513-f3a8-46f7-8f46-97d3b86cfcff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWepDevSword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45d8bd19-6a81-4810-8209-ce7a6c8869c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51fe5513-f3a8-46f7-8f46-97d3b86cfcff",
            "compositeImage": {
                "id": "77055d4c-1741-433c-8f0a-945e77d10c2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d8bd19-6a81-4810-8209-ce7a6c8869c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93285d2b-4594-45b0-97f1-ee59879699db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d8bd19-6a81-4810-8209-ce7a6c8869c0",
                    "LayerId": "4d326a85-eaaf-4a32-a5dc-1aa356075ea5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "4d326a85-eaaf-4a32-a5dc-1aa356075ea5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51fe5513-f3a8-46f7-8f46-97d3b86cfcff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 29
}