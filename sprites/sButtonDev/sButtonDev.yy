{
    "id": "47184a59-c800-4228-9412-9d1c47abd227",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButtonDev",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7d1df20-8fdb-41c8-a1fb-9d4812fde896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47184a59-c800-4228-9412-9d1c47abd227",
            "compositeImage": {
                "id": "aec79bc1-7f93-4a31-a96f-4c456223e413",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7d1df20-8fdb-41c8-a1fb-9d4812fde896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96f25a5c-d31a-4b8f-b9de-bb86098689a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7d1df20-8fdb-41c8-a1fb-9d4812fde896",
                    "LayerId": "183bbbae-29d8-471b-bb4b-46cd9edd2c7b"
                }
            ]
        },
        {
            "id": "1b0ee60b-6684-4c5d-aba3-af24874cd131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47184a59-c800-4228-9412-9d1c47abd227",
            "compositeImage": {
                "id": "e0bc5d8c-730e-42e8-9a54-75fc22eaa029",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b0ee60b-6684-4c5d-aba3-af24874cd131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24d7232b-aa45-49d4-be92-c1ae1b822e06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b0ee60b-6684-4c5d-aba3-af24874cd131",
                    "LayerId": "183bbbae-29d8-471b-bb4b-46cd9edd2c7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "183bbbae-29d8-471b-bb4b-46cd9edd2c7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47184a59-c800-4228-9412-9d1c47abd227",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 22,
    "yorig": 24
}