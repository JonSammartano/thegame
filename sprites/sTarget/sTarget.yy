{
    "id": "2a0e20f3-9e58-43db-92fe-99ea4f23c64b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTarget",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5f2b27d-b340-4634-9581-68e875efdb10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0e20f3-9e58-43db-92fe-99ea4f23c64b",
            "compositeImage": {
                "id": "19366b11-d688-4db8-a08e-1f70420ce391",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5f2b27d-b340-4634-9581-68e875efdb10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b05255f9-26f8-4cd2-bb6c-5aa190f11ea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5f2b27d-b340-4634-9581-68e875efdb10",
                    "LayerId": "4f966369-b4f5-4088-bbfc-9aaa7e3a355e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4f966369-b4f5-4088-bbfc-9aaa7e3a355e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a0e20f3-9e58-43db-92fe-99ea4f23c64b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}