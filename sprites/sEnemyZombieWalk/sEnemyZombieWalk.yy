{
    "id": "9eda1b85-cd5a-4eb8-83c8-97fae1b5c376",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyZombieWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74dd69dd-e290-4023-a44a-94ead884045e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eda1b85-cd5a-4eb8-83c8-97fae1b5c376",
            "compositeImage": {
                "id": "6fca6799-210a-4ac0-912c-90cf40e2f502",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74dd69dd-e290-4023-a44a-94ead884045e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4be67bb0-0adb-45c3-a889-4d844aed6b2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74dd69dd-e290-4023-a44a-94ead884045e",
                    "LayerId": "a04c50cc-851a-4b26-afeb-a4364e853dae"
                }
            ]
        },
        {
            "id": "5ecaa714-1250-4f21-bdb2-36380b74d81a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eda1b85-cd5a-4eb8-83c8-97fae1b5c376",
            "compositeImage": {
                "id": "f3af6f81-1a44-4727-85ec-450d69910bcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ecaa714-1250-4f21-bdb2-36380b74d81a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dce1cb62-2757-453a-82a6-5a757cdf201c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ecaa714-1250-4f21-bdb2-36380b74d81a",
                    "LayerId": "a04c50cc-851a-4b26-afeb-a4364e853dae"
                }
            ]
        },
        {
            "id": "2d6f193b-44fe-4562-8823-a63e98ac7228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eda1b85-cd5a-4eb8-83c8-97fae1b5c376",
            "compositeImage": {
                "id": "665f2e30-554b-4505-89a0-f79693f414b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d6f193b-44fe-4562-8823-a63e98ac7228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "016084ee-df49-4cf1-a62b-386b0fe61a4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d6f193b-44fe-4562-8823-a63e98ac7228",
                    "LayerId": "a04c50cc-851a-4b26-afeb-a4364e853dae"
                }
            ]
        },
        {
            "id": "931c1a49-73f4-4f2b-8693-5e8300df8588",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9eda1b85-cd5a-4eb8-83c8-97fae1b5c376",
            "compositeImage": {
                "id": "ae4c44b2-47af-4383-87cb-3e46af66dda1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "931c1a49-73f4-4f2b-8693-5e8300df8588",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c382a8-8a66-4bfb-b7e7-a5d983f22bc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "931c1a49-73f4-4f2b-8693-5e8300df8588",
                    "LayerId": "a04c50cc-851a-4b26-afeb-a4364e853dae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "a04c50cc-851a-4b26-afeb-a4364e853dae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9eda1b85-cd5a-4eb8-83c8-97fae1b5c376",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 17
}