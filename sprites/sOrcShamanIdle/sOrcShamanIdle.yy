{
    "id": "6f702c92-9d27-4592-bdd8-8a9226f369b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOrcShamanIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 4,
    "bbox_right": 14,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5343f4b-2e9e-41b3-8b9d-4ea99c93e875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f702c92-9d27-4592-bdd8-8a9226f369b7",
            "compositeImage": {
                "id": "c97b525a-e2f9-4466-8f03-7322f60b7aa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5343f4b-2e9e-41b3-8b9d-4ea99c93e875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3c6cfb7-8367-4302-9e8e-2fcbd53918af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5343f4b-2e9e-41b3-8b9d-4ea99c93e875",
                    "LayerId": "ceac444a-60e5-4564-a019-61fcac6b393f"
                }
            ]
        },
        {
            "id": "9bb3f58d-7b56-4e46-b232-8817569442b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f702c92-9d27-4592-bdd8-8a9226f369b7",
            "compositeImage": {
                "id": "fb46f67c-9d08-4d78-a789-8269923de28f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bb3f58d-7b56-4e46-b232-8817569442b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a41eb37b-7350-4c16-a245-2dc884665687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bb3f58d-7b56-4e46-b232-8817569442b0",
                    "LayerId": "ceac444a-60e5-4564-a019-61fcac6b393f"
                }
            ]
        },
        {
            "id": "f82779bf-6aaf-4252-a310-e75bc19eabf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f702c92-9d27-4592-bdd8-8a9226f369b7",
            "compositeImage": {
                "id": "7825239f-4a60-4cca-b470-e7583692c5a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f82779bf-6aaf-4252-a310-e75bc19eabf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e28b566-9e02-4f5f-be60-e00374562561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f82779bf-6aaf-4252-a310-e75bc19eabf0",
                    "LayerId": "ceac444a-60e5-4564-a019-61fcac6b393f"
                }
            ]
        },
        {
            "id": "d3a3d41d-575b-4983-9e07-7e4a9dd599a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f702c92-9d27-4592-bdd8-8a9226f369b7",
            "compositeImage": {
                "id": "df71d926-a135-402b-ba08-62be178e99c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a3d41d-575b-4983-9e07-7e4a9dd599a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a4bc756-6e02-4732-9152-46d05102e098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a3d41d-575b-4983-9e07-7e4a9dd599a8",
                    "LayerId": "ceac444a-60e5-4564-a019-61fcac6b393f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "ceac444a-60e5-4564-a019-61fcac6b393f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f702c92-9d27-4592-bdd8-8a9226f369b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 10
}