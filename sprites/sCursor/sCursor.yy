{
    "id": "c45d7318-29a4-458b-abbc-d5883993171e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae3e1cb0-ea70-45ce-aeb4-003d2d01f5ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c45d7318-29a4-458b-abbc-d5883993171e",
            "compositeImage": {
                "id": "ecf5e7f1-3944-47e4-9ca3-a023c5735d97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae3e1cb0-ea70-45ce-aeb4-003d2d01f5ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87df0137-3e44-4fc9-88aa-d0cb020f1867",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae3e1cb0-ea70-45ce-aeb4-003d2d01f5ce",
                    "LayerId": "8d54350b-f740-4dbb-99d6-72a25d5e339e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "8d54350b-f740-4dbb-99d6-72a25d5e339e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c45d7318-29a4-458b-abbc-d5883993171e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 2,
    "yorig": 2
}