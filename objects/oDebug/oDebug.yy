{
    "id": "37a95304-cd0c-4a50-892f-0657e64e7a3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDebug",
    "eventList": [
        {
            "id": "7ded5b55-8d82-4840-90ce-b95e6089cdb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "37a95304-cd0c-4a50-892f-0657e64e7a3b"
        },
        {
            "id": "8c9c593b-6d66-4156-b219-e75f2a5fe79a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 114,
            "eventtype": 10,
            "m_owner": "37a95304-cd0c-4a50-892f-0657e64e7a3b"
        },
        {
            "id": "c31c5bcf-d1e0-40f1-a8a9-af9551801a66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "37a95304-cd0c-4a50-892f-0657e64e7a3b"
        },
        {
            "id": "ad28156e-48ce-4537-b57b-ee2e12e557d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "37a95304-cd0c-4a50-892f-0657e64e7a3b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}