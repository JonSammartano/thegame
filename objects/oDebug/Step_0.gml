/// @description 

//if alarm[0] <= 0 { alarm[0] = 0.5 * room_speed; }

timer -= (1 / global.desiredfps) * global.framedelta;
if timer <= 0 {
	timer = 0.5;


	scrDebugClear();

	if global.debugListDraw {
		scrDebugAdd("-DEBUG MENU F3 TO TOGGLE-");
		scrDebugAdd("fps: " + string(fps));
		scrDebugAdd("fps_real: " + string(fps_real));
		scrDebugAdd("objectHoverName: " + string(global.objectHoverName));
		scrDebugAdd("objectHoverID: " + string(global.objectHoverID));
		scrDebugAdd("objectSelectedName: " + string(global.objectSelectedName));
		scrDebugAdd("objectSelectedID: " + string(global.objectSelectedID));
		scrDebugAdd("objectIsSelected: " + string(global.objectIsSelected));
		scrDebugAdd("nsfs status: " + string(nsfs_status));
		scrDebugAdd("delta: " + string(global.delta));
	}

}