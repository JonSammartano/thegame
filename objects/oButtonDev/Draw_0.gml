/// @description WITH THIS

draw_self(); // if you're drawing ABOVE the sprite then code below this

#region draw label
/*** if you want to completely change the code in this region please just comment it out ***/
var stringText = tmpval;

draw_set_halign(fa_center);
draw_set_font(fontDevbig);
draw_set_color(c_black);
draw_text(x, y - (string_height(stringText) / 2), stringText);
#endregion
