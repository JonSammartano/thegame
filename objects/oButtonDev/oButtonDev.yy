{
    "id": "ea6e4ac2-df02-4a08-8796-7cbef4891a27",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oButtonDev",
    "eventList": [
        {
            "id": "4415ed3a-d0f5-41b8-aa9b-4e9c0fbc56f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ea6e4ac2-df02-4a08-8796-7cbef4891a27"
        },
        {
            "id": "b4338369-5d6c-4094-b431-22bccad9371f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "ea6e4ac2-df02-4a08-8796-7cbef4891a27"
        },
        {
            "id": "fe7ae5e2-c030-44c3-b6cd-5cbcfd68aa21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ea6e4ac2-df02-4a08-8796-7cbef4891a27"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "47184a59-c800-4228-9412-9d1c47abd227",
    "visible": true
}