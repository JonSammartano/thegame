{
    "id": "8449ff46-dc17-4c86-ba67-91e2b5babb32",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oConsole",
    "eventList": [
        {
            "id": "467dd255-ada5-46ac-b4ab-54afe76b27ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8449ff46-dc17-4c86-ba67-91e2b5babb32"
        },
        {
            "id": "024f61c1-551a-4ae4-83b7-42794c554766",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8449ff46-dc17-4c86-ba67-91e2b5babb32"
        },
        {
            "id": "15a7c879-bc9a-42a5-b322-b93f1927df28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 112,
            "eventtype": 9,
            "m_owner": "8449ff46-dc17-4c86-ba67-91e2b5babb32"
        },
        {
            "id": "2c32f68b-6eb3-4c94-924d-cdea92536b59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8449ff46-dc17-4c86-ba67-91e2b5babb32"
        },
        {
            "id": "7debc23f-84c3-486e-8ad7-507bd8f2be6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "8449ff46-dc17-4c86-ba67-91e2b5babb32"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}