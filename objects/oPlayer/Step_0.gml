/// @description Insert description here
// You can write your code in this editor


if state != objectState.paused {
	#region variable init
	var up = keyboard_check(global.keyUp);
	var down = keyboard_check(global.keyDown);
	var left = keyboard_check(global.keyLeft);
	var right = keyboard_check(global.keyRight);
	var slot1 = keyboard_check_pressed(global.keySlot1);
	var slot2 = keyboard_check_pressed(global.keySlot2);
	var slot3 = keyboard_check_pressed(global.keySlot3);
	var openbag = keyboard_check_pressed(global.keyOpenBag);
	var currentSpeed = 0;
	var currentDirection = 0;
	state = objectState.idle;
	#endregion
	

	#region walking controls
	if up {currentDirection = 90; currentSpeed = 2; state = objectState.walking;}
	if left {currentDirection = 180; currentSpeed = 2; image_xscale = -1; state = objectState.walking;}
	if down {currentDirection = 270; currentSpeed = 2; state = objectState.walking;}
	if right {currentDirection = 0; currentSpeed = 2; image_xscale = 1; state = objectState.walking;}
	if up & left {currentDirection = 135; currentSpeed = 2; image_xscale = -1; state = objectState.walking;}
	if up & right {currentDirection = 45; currentSpeed = 2; image_xscale = 1; state = objectState.walking;}
	if down & left {currentDirection = 225; currentSpeed = 2; image_xscale = -1; state = objectState.walking;}
	if down & right {currentDirection = 315; currentSpeed = 2; image_xscale = 1; state = objectState.walking;}
	scrMove(id, currentSpeed, currentDirection);
	#endregion
	
	#region combat controls
	if slot1 {scrHurt(global.objectSelectedID, 10);}
	if slot2 {log("keybind works for slot2");}
	if slot3 {log("keybind works for slot3");}
	#endregion
	
	#region general controls
	if openbag {
		if !instance_exists(oBag) {
			instance_create_layer(0, 0, "instUI", oBag);
		} else {
			instance_destroy(oBag);	
		}
	}
	#endregion
	
	if (mouse_check_button_pressed(mb_right)){
		global.objectIsSelected = false;
		global.objectSelectedID = "null";
		global.objectSelectedName = "null";
	}
}
