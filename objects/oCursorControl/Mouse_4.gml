/// @description 
with instance_position(mouse_x, mouse_y, oCursorControl) {	
	if object_get_name(object_index) != global.objectSelectedName {
		any = 1;
		global.objectIsSelected = true;
		global.objectSelectedID = id;
		global.objectSelectedName = object_get_name(object_index);
	}else{
		global.objectIsSelected = false;
		global.objectSelectedID = "null";
		global.objectSelectedName = "null";
	}
}