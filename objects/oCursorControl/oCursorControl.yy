{
    "id": "b849430a-5540-414d-8a79-05aa585c9e48",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCursorControl",
    "eventList": [
        {
            "id": "1bb7b418-d1c3-491e-bc4e-acc8d75955bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b849430a-5540-414d-8a79-05aa585c9e48"
        },
        {
            "id": "35afbca4-182d-4ced-adc8-740be95d91bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 6,
            "m_owner": "b849430a-5540-414d-8a79-05aa585c9e48"
        },
        {
            "id": "35ce2e0e-29f8-4106-acaa-4536c4d5adb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b849430a-5540-414d-8a79-05aa585c9e48"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}