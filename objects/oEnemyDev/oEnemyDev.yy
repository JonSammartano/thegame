{
    "id": "9d5c3587-3361-4468-8e03-523c3805dfca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyDev",
    "eventList": [
        {
            "id": "f2cdb163-3036-4548-a7b6-eb73c20c8b33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9d5c3587-3361-4468-8e03-523c3805dfca"
        },
        {
            "id": "07296090-dd3d-4f37-81fb-192beca72ab7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9d5c3587-3361-4468-8e03-523c3805dfca"
        },
        {
            "id": "40b05107-eb64-4ec7-86bf-e2ccc8f887ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9d5c3587-3361-4468-8e03-523c3805dfca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b849430a-5540-414d-8a79-05aa585c9e48",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "35d861fd-3723-4e91-9998-b2fef6cb7ca6",
    "visible": true
}