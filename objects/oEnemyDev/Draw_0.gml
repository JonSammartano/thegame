/// @description 

scrDrawHealthAndMana(id);

switch state {
	case objectState.idle:
		sprite_index = sEnemyZombieIdle;
		image_speed = 0.5;
	break;
	
	case objectState.walking:
		sprite_index = sEnemyZombieWalk;
		image_speed = 1;
	break;
}

switch facingDir {
	case facing.left:
		image_xscale = -1;
	break;
	case facing.right:
		image_xscale = 1;
	break;
}

if oldx > x {
	facingDir = facing.left;
	state = objectState.walking;
} else if oldx < x {
	facingDir = facing.right;
	state = objectState.walking;
} else if oldx = x {
	state = objectState.idle;	
}

oldx = x;
draw_self();