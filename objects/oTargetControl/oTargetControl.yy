{
    "id": "2fd403e2-60fb-46a6-a736-1a19a9c55e5b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTargetControl",
    "eventList": [
        {
            "id": "f0e2a954-1a9a-428b-83e6-5de0f5875864",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2fd403e2-60fb-46a6-a736-1a19a9c55e5b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a0e20f3-9e58-43db-92fe-99ea4f23c64b",
    "visible": true
}