global.worldGrid = ds_grid_create((room_width div 16), (room_height div 16));

with oCollisionHitbox {
	ds_grid_set(global.worldGrid, (x div 16), (y div 16), 1);
	instance_destroy();
}