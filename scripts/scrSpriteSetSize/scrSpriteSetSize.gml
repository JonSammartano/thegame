/// @description set sprite to specific pixel size
/// @arg0 = sender
/// @arg1 = width
/// @arg2 = height
/// @arg3 = sprite
// probably will fuck something up horribly if ever used
var sender = argument0;
var width = argument1;
var height = argument2;
var sprite = argument3;

sender.image_xscale = (sender.image_xscale / sprite_get_width(sprite)) * width;
sender.image_yscale = (sender.image_yscale / sprite_get_height(sprite)) * height;