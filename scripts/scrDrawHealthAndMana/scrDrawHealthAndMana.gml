var target = argument0;
//var targetSprite = object_get_sprite(target);

var x1 = target.x - abs(target.sprite_width) / 2;
var y1 = target.y - target.sprite_height;
var x2 = x1 + abs(target.sprite_width);
var y2 = y1 + 4;

var y3 = y2 + 2;
var y4 = y3 + 4;

var percentageToDraw = scrCalculatePercentage(target.hpCurrent, target.hpMax);

draw_healthbar(x1, y1, x2, y2, percentageToDraw, c_black, c_green, c_green, 0, true, true);
draw_healthbar(x1, y3, x2, y4, percentageToDraw, c_black, c_blue, c_blue, 0, true, true);