//slice(sprite_index,x1,y1,x2,y2)

//takes a square image and divides it into a 3x3 grid to be used with GUI and scaling boxes
//used in things like text-boxes, menus, etc.
//basically it scales the box without all the crappy blurry distortion
//made by hopeabandoner
var neww = sprite_get_width(argument0)/3;
var newh = sprite_get_height(argument0)/3;
var xdist = argument3 - argument1;
var ydist = argument4 - argument2;

draw_sprite_part_ext(argument0,0,0,0,neww,newh,argument1,argument2,1,1,c_white,1) //top left
draw_sprite_part_ext(argument0,0,neww*1,0,neww,newh,argument1+neww*1,argument2,(xdist-neww)/neww,1,c_white,1) //top mid
draw_sprite_part_ext(argument0,0,neww*2,0,neww,newh,argument3,argument2,1,1,c_white,1) //top right

draw_sprite_part_ext(argument0,0,0,neww*1,neww,newh,argument1,argument2+newh*1,1,(ydist-newh*2)/newh,c_white,1) //mid left
draw_sprite_part_ext(argument0,0,neww*1,neww*1,neww,newh,argument1+neww*1,argument2+newh*1,(xdist-neww)/neww,(ydist-newh*2)/newh,c_white,1) //mid mid
draw_sprite_part_ext(argument0,0,neww*2,neww*1,neww,newh,argument3,argument2+newh,1,(ydist-newh*2)/newh,c_white,1) //mid right

draw_sprite_part_ext(argument0,0,0,newh*2,neww,newh*2,argument1,argument2+ydist-newh,1,1,c_white,1) //bot left
draw_sprite_part_ext(argument0,0,neww*1,newh*2,neww,newh,argument1+neww*1,argument2+ydist-newh,(xdist-neww)/neww,1,c_white,1) //bot mid
draw_sprite_part_ext(argument0,0,neww*2,newh*2,neww,newh,argument3,argument2+ydist-newh,1,1,c_white,1) //bot right