var origin = argument0;
var target = argument1;
var amount = argument2;
var dir = point_direction(origin.x, origin.y, target.x, target.y);
scrMove(target, amount, dir);