/// @desc _draw_debug_info(ds_list(of things to draw))
/// @param val1 list of things to draw
var drawList = argument0
var listIndex = 0;
var listIndexFirst = 0;
var listSize = ds_list_size(drawList);
var yy = 0;
var yMax = listSize * 10;


draw_set_font(fontDebug);
draw_set_halign(fa_left);
draw_set_color(c_yellow);

for (listIndex = 0; listIndex < listSize; listIndex++) {
	var _list_item = drawList[| listIndex];
	scrDrawTextOutlined(0, yy, c_black, c_yellow, string(_list_item));
	yy += string_height(string(_list_item));
}

