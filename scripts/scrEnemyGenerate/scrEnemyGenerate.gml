sender = argument0; // should be sender id
with sender {
	hpMax = 100;
	mpMax = 100;
	hpCurrent = 100;
	mpCurrent = 100;
	attack = 1;
	defense = 1;
	magic = 1;
	state = objectState.idle
	oldx = x;
	startposx = x;
	facingDir = facing.left;
}