/// initialization required for the entire game

game_set_speed(16666, gamespeed_microseconds);

#region initialize directories and files

#macro dirMyGames (environment_get_variable("USERPROFILE") + "\\Documents\\My Games")
#macro configDirectory (dirMyGames + "\\Untitled")
#macro configFile (configDirectory + "\\cfg.ini")
if !directory_exists_ns(dirMyGames) { directory_create_ns(dirMyGames); }
if !directory_exists_ns(configDirectory) { directory_create_ns(configDirectory); }

#endregion



global.debugList = ds_list_create();
global.debugListDraw = false;

/*global.tileIsSelected = false;
global.tileSelectedID = "null";
global.tileSelectedName = "null";		possible vars for map editor
global.tileHoverID = "null";
global.tileHoverName = "null";*/

global.objectIsSelected = false; // is object selected
global.objectSelectedID = "null"; // id of selected object
global.objectSelectedName = "null"; // name of selected object
global.objectHoverID = "null"; // id of object mouse is hovering
global.objectHoverName = "null"; // name of object mouse is hovering

#region enums
enum facing {
	left,
	right
}

enum cursorState {
	normal,
	attack
}

enum objectState {
	paused,
	idle,
	walking
}

#endregion


initCfg()

audio_master_gain(0.25);
window_set_caption("build date " + date_time_string(GM_build_date) + ", runtime " + GM_runtime_version + ", version " + GM_version);
window_set_cursor(cr_none);
cursor_sprite = sCursor;

room_goto(rDevRoom);