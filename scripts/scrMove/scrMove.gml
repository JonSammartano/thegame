/// @arg id
/// @arg speed
/// @arg direction

var sender = argument0;
var spd = argument1;
var dir = argument2;
var xtarg = sender.x+lengthdir_x(spd,dir);
var ytarg = sender.y+lengthdir_y(spd,dir);
 
/*if place_free(xtarg,ytarg) {	OLD CODE DO NOT USE DOES NOT DETECT COLLISION.
    x = xtarg;					OLD CODE DO NOT USE DOES NOT DETECT COLLISION.
    y = ytarg;					OLD CODE DO NOT USE DOES NOT DETECT COLLISION.
}*/

if ds_grid_get(global.worldGrid, (xtarg div 16), (ytarg div 16)) = 0 {
	sender.x = xtarg;
	sender.y = ytarg;
}
else {
    var sweep_interval = 10;
    
    for ( var angle = sweep_interval; angle <= 80; angle += sweep_interval) {
        for ( var multiplier = -1; multiplier <= 1; multiplier += 2) {      
            var angle_to_check = dir+angle*multiplier;
            xtarg = sender.x+lengthdir_x(spd, angle_to_check);
            ytarg = sender.y+lengthdir_y(spd, angle_to_check);     
            if ds_grid_get(global.worldGrid, (xtarg div 16), (ytarg div 16)) = 0 {
                sender.x = xtarg;
                sender.y = ytarg;  
                exit;       
            }   
        }
    }
}