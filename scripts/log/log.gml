/// log(_message)
/// @arg _message

var _log_file = configDirectory + "\\log.txt";
var _time = get_timer() / 1000;
var _message = string(_time) + ": -" + argument0;

show_debug_message(_message);
file_text_open_append_ns(_log_file);
file_text_write_line_ns(_log_file, _message + "\n");
file_text_close_ns(_log_file);