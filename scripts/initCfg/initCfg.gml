// the nsfs extension lets you unsandbox files to store permenantly on disk
// POWERSHELL: Get-ChildItem Env:

/// SYSTEM ERROR CODES https://docs.microsoft.com/en-us/windows/desktop/Debug/system-error-codes--0-499-
log("Begin loading configuration files");
if (!nsfs_is_available) {
	log("nsfs_status: " + nsfs_status);
	game_end();
}

ini_open_ns(configFile); // you can open an ini if its not created
if !file_exists_ns(configFile) { ini_write_real("init", "done", false); }

ini_open_ns(configFile);
if ini_read_real("init", "done", false) == false {
	ini_write_real("controls", "up", ord("W"));
	ini_write_real("controls", "down", ord("S"));
	ini_write_real("controls", "left", ord("A"));
	ini_write_real("controls", "right", ord("D"));
	ini_write_real("controls", "slot1", ord("1"));
	ini_write_real("controls", "slot2", ord("2"));
	ini_write_real("controls", "slot3", ord("3"));
	ini_write_real("controls", "openbag", ord("B"));
}

#region load all settings

global.keyUp = ini_read_real(configFile, "up", ord("W"));
global.keyDown = ini_read_real(configFile, "down", ord("S"));
global.keyLeft = ini_read_real(configFile, "left", ord("A"));
global.keyRight = ini_read_real(configFile, "right", ord("D"));
global.keySlot1 = ini_read_real(configFile, "slot1", ord("1"));
global.keySlot2 = ini_read_real(configFile, "slot2", ord("2"));
global.keySlot3 = ini_read_real(configFile, "slot3", ord("3"));
global.keyOpenBag = ini_read_real(configFile, "openbag", ord("B"));
ini_write_real("init", "done", true);
ini_close_ns();
log("End loading configuration files");
#endregion

