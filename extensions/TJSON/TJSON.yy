{
    "id": "81574004-cb6f-416f-af54-f48cfadbf239",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "TJSON",
    "IncludedResources": [
        "Scripts\\scr_tjson_demo",
        "Rooms\\rm_tjson_demo",
        "Included Files\\tjson-doc.html"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 105554172285166,
    "date": "2019-37-24 04:02:33",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "70f2eff1-de36-47a5-877d-fb21a06fb180",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                {
                    "id": "56010d2d-2ef3-4794-8f73-7618a5456f9e",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_error_text",
                    "hidden": false,
                    "value": "g_tj_error_text"
                },
                {
                    "id": "35ad5fff-07a3-4f6e-a300-ff7f619040eb",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_true",
                    "hidden": false,
                    "value": "g_tj_true"
                },
                {
                    "id": "4a54a5db-ed2e-4f4b-907c-538f17592573",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_false",
                    "hidden": false,
                    "value": "g_tj_false"
                },
                {
                    "id": "9a55798b-87f0-4436-b975-cfe34a22eb11",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_error",
                    "hidden": false,
                    "value": "g_tj_error"
                },
                {
                    "id": "4ba4bae9-4406-4764-85df-3857861af804",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_null",
                    "hidden": false,
                    "value": "undefined"
                },
                {
                    "id": "a3db1e55-521e-432d-8aea-34b957d2477b",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_separator",
                    "hidden": true,
                    "value": "chr(27)"
                },
                {
                    "id": "971d8d09-a5d0-45b5-8fff-3e18efb23880",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_undefined",
                    "hidden": false,
                    "value": "0"
                },
                {
                    "id": "79a613e1-ec1e-472c-9d28-a6f4e24736cd",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_boolean",
                    "hidden": false,
                    "value": "1"
                },
                {
                    "id": "505f47c2-a84b-4381-b951-127cf2a6d438",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_number",
                    "hidden": false,
                    "value": "2"
                },
                {
                    "id": "68120c6c-e74c-4e3c-814e-f1b6c932f95f",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_string",
                    "hidden": false,
                    "value": "3"
                },
                {
                    "id": "a3665774-fdc5-428d-b9a6-dc30dfc2f040",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_array",
                    "hidden": false,
                    "value": "4"
                },
                {
                    "id": "ac3dfbb9-de50-4c90-8623-0748dae466a3",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_object",
                    "hidden": false,
                    "value": "5"
                },
                {
                    "id": "6d1112a0-4d03-4a9f-b01c-ef849d3507c8",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_error",
                    "hidden": false,
                    "value": "6"
                },
                {
                    "id": "60e0b630-30e8-423c-9ae1-43798e749dd1",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_key_sep",
                    "hidden": true,
                    "value": "27"
                },
                {
                    "id": "9118bb56-bf2e-4630-b3f1-4e7365e323f4",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_key_true",
                    "hidden": true,
                    "value": "35554"
                },
                {
                    "id": "5ed825e4-d4f9-4a9a-9771-093f9c463ed8",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_key_false",
                    "hidden": true,
                    "value": "35554"
                },
                {
                    "id": "85b868d3-ae52-4ee7-9430-4c850a466f4a",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_key_null",
                    "hidden": true,
                    "value": "37090"
                },
                {
                    "id": "0dac2cdf-510f-4b87-8254-9e8df4212bcb",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_key_number",
                    "hidden": true,
                    "value": "35554"
                },
                {
                    "id": "bdf48dfc-dc0a-461e-a31a-5962f2fd6abc",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_unknown",
                    "hidden": false,
                    "value": "7"
                },
                {
                    "id": "41200f43-5fbc-4754-8a61-d3eb9264c20f",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_decode_esc",
                    "hidden": true,
                    "value": "92"
                }
            ],
            "copyToTargets": 113497714200782,
            "filename": "tjson-native.gml",
            "final": "",
            "functions": [
                {
                    "id": "05206272-77e1-40ae-a41e-9b9f9d572a64",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "tj_init_gml",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_init_gml",
                    "returnType": 2
                },
                {
                    "id": "9c067253-7ed9-463e-9997-23fd87d64c2f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        2,
                        2,
                        2,
                        2
                    ],
                    "externalName": "tj_stringbuf_add_sub",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_stringbuf_add_sub",
                    "returnType": 2
                },
                {
                    "id": "979aa266-201d-4451-868d-3f25e25c173c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_stringbuf_to_string",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_stringbuf_to_string",
                    "returnType": 2
                },
                {
                    "id": "f192a6ef-4ecd-4be9-b186-e91b1c23a9bc",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "tj_opt_value_keys",
                    "help": "tj_opt_value_keys(?allow:bool)->bool : [native-only] Returns\/changes whether keys can be non-string (non-spec-compliant)",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_opt_value_keys",
                    "returnType": 2
                },
                {
                    "id": "4e4a0c46-d214-4153-8efc-95007954b5d1",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_uxchar",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_uxchar",
                    "returnType": 2
                },
                {
                    "id": "7e945d7f-e184-47f6-92ae-ec3700ff79e2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "tj_decode_string",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_decode_string",
                    "returnType": 2
                },
                {
                    "id": "eb8204e1-7ffd-426f-a4b9-be3e61d2d45a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_decode_number",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_decode_number",
                    "returnType": 2
                },
                {
                    "id": "b0f95d80-b989-4c70-81f6-9847408f1c26",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "tj_decode_value",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_decode_value",
                    "returnType": 2
                },
                {
                    "id": "ba3d4732-b431-4aa2-8583-cb54704e2571",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_decode",
                    "help": "tj_decode(json:string)->TjValue",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_decode",
                    "returnType": 2
                },
                {
                    "id": "12cff0ad-8b19-4509-ba6c-f8ba5683c9d9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "tj_encode_string",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_encode_string",
                    "returnType": 2
                },
                {
                    "id": "140cf741-ea8e-464c-9c5f-0f0e3a58946e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        2,
                        2,
                        2,
                        2
                    ],
                    "externalName": "tj_encode_value",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_encode_value",
                    "returnType": 2
                },
                {
                    "id": "1a112c2c-faa1-4892-8369-0252335b8ddc",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "tj_encode",
                    "help": "tj_encode(value:TjValue, ?indent:string)->string",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_encode",
                    "returnType": 2
                },
                {
                    "id": "e36358f3-2759-4dd9-8ebc-30a9f81abc06",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_source__new",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_source__new",
                    "returnType": 2
                },
                {
                    "id": "79dca51a-94ab-4cfc-9c3b-2d6ffe8d6a37",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        2,
                        2,
                        2
                    ],
                    "externalName": "tj_source_sub",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_source_sub",
                    "returnType": 2
                },
                {
                    "id": "57d88ce5-3d77-4871-9e32-1727be8f2dc8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_is_array",
                    "help": "tj_is_array(q:dynamic)->bool : Returns whether a value is a TJSON array",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_is_array",
                    "returnType": 2
                },
                {
                    "id": "184fc4bf-926b-4f1d-bce7-367cc9096491",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_is_object",
                    "help": "tj_is_object(q:dynamic)->bool : Returns whether a value is a TJSON object",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_is_object",
                    "returnType": 2
                },
                {
                    "id": "075e74fe-5d55-4412-b827-b8ec70fe142e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_magic_value",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_magic_value",
                    "returnType": 2
                },
                {
                    "id": "3a6db203-83e7-4227-80a5-f5a2b3966893",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_bool",
                    "help": "tj_bool(v:bool)->TjValue",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_bool",
                    "returnType": 2
                },
                {
                    "id": "06b151b6-3f59-4439-9df6-f71855291b7d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_is_bool",
                    "help": "tj_is_bool(v:TjValue)->bool",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_is_bool",
                    "returnType": 2
                },
                {
                    "id": "94335fc2-4ed0-4573-899e-15af5990260b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_typeof",
                    "help": "tj_typeof(q:TjValue)->string : Returns a type of given TJSON value as string.",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_typeof",
                    "returnType": 2
                },
                {
                    "id": "33e53045-a3b1-46e9-8f91-3e93f403ace8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_typeid",
                    "help": "tj_typeid(q:TjValue)->string : Returns a type of given TJSON value as tj_type.",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_typeid",
                    "returnType": 2
                },
                {
                    "id": "cc7bd5d4-9bab-4d71-9687-0902a4094c1e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "tj_object_empty_init",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_object_empty_init",
                    "returnType": 2
                },
                {
                    "id": "e771bdd3-b2eb-4f98-9132-c51db4a6fa0b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "tj_object",
                    "help": "tj_object(...key_value_pairs:TjValue)->TjValue : Creates a TJSON object from pairs of provided keys and values",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_object",
                    "returnType": 2
                },
                {
                    "id": "81e3f035-dd72-4dda-9cdd-2b9d38b77e97",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "tj_array",
                    "help": "tj_array(...values:TjValue)->array<TjValue> : Crates an TJSON array from the provided arguments.",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_array",
                    "returnType": 2
                },
                {
                    "id": "15bad8ac-7abc-4459-9428-1f4381e74ee6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "tj_get",
                    "help": "tj_get(q:TjValue, key:string)->TjValue : Retrieves a field out of a TJSON object.",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_get",
                    "returnType": 2
                },
                {
                    "id": "389d15b4-b4ce-4dd4-889e-58c05c2c061c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        2,
                        2,
                        2
                    ],
                    "externalName": "tj_set",
                    "help": "tj_set(q:TjValue, key:string, val:TjValue) : Changes a field in the given TJSON object.",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_set",
                    "returnType": 2
                },
                {
                    "id": "8e4116ae-fff2-4d0c-8deb-17e0b440eb4a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_size",
                    "help": "tj_size(q:TjValue)->int : Returns the number of fields on given TJSON object, 0 if it isn't an object.",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_size",
                    "returnType": 2
                },
                {
                    "id": "f211abfd-f63a-40cc-95d7-ce4ddae2a286",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_keys",
                    "help": "tj_keys(q:TjValue)->array<string> : Returns an array containing the fields of the given TJSON object.",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_keys",
                    "returnType": 2
                },
                {
                    "id": "9e7f5b8c-c002-45fb-a8a1-788874b1b040",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "tj_compare",
                    "help": "tj_compare(a:TjValue, b:TjValue)->bool : Recursively compares whether the two values match.",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_compare",
                    "returnType": 2
                },
                {
                    "id": "341dbc58-1922-4039-aba2-d8f1de9a9b3b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "tj_encode_float",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_encode_float",
                    "returnType": 2
                },
                {
                    "id": "d76357ab-8844-4f9c-94e9-84d4dd54088a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "tj_object_wkey",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_object_wkey",
                    "returnType": 2
                },
                {
                    "id": "a3869ddd-3ade-4237-904c-45e359665dd8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_decode_uxchar",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_decode_uxchar",
                    "returnType": 2
                },
                {
                    "id": "f8197676-0a0e-4ecf-8db1-6fbbe5407592",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_decode_main",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_decode_main",
                    "returnType": 2
                },
                {
                    "id": "fa800c06-db92-4111-b3b7-6b7eedf54ecf",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "tj_encode_number",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_encode_number",
                    "returnType": 2
                },
                {
                    "id": "deaea80d-76b0-4b5b-96c9-8323be6c6bbd",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "tj_encode_main",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_encode_main",
                    "returnType": 2
                },
                {
                    "id": "3e6bab7c-e8cf-4501-a64d-230125676958",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_copy",
                    "help": "tj_copy(val:TjValue)->TjValue : Recursively copies the given TJSON value and returns it",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_copy",
                    "returnType": 2
                }
            ],
            "init": "tj_init_gml",
            "kind": 2,
            "order": [
                
            ],
            "origname": "tjson-native.gml",
            "uncompress": false
        },
        {
            "id": "e344cdeb-3de7-41f4-9e3d-22679e6247e7",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                {
                    "id": "7659c1aa-8cfc-4a99-857b-f47806b5cabe",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_error_text",
                    "hidden": true,
                    "value": "tj_get_error_text()"
                }
            ],
            "copyToTargets": 98336,
            "filename": "tjson-web.js",
            "final": "",
            "functions": [
                {
                    "id": "54000d5c-f5ad-482d-abbc-ad71fc389037",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "tj_get_error_value",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_get_error_value",
                    "returnType": 2
                },
                {
                    "id": "3108797f-1154-47d6-8674-4027f9f61ab6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "tj_get_error_text",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_get_error_text",
                    "returnType": 2
                },
                {
                    "id": "d5d37467-24ad-441a-9824-b6a268c5f425",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "tj_get_null_value",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_get_null_value",
                    "returnType": 2
                },
                {
                    "id": "48ffdb2c-235a-4ee2-bb4b-6eac1b6dc56c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_bool",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_bool",
                    "returnType": 2
                },
                {
                    "id": "fc8331c7-ecca-4e4f-996e-7bf87789690b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_is_bool",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_is_bool",
                    "returnType": 2
                },
                {
                    "id": "79a2f67a-0992-4b30-b41f-58d8b206eedc",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "tj_object",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_object",
                    "returnType": 2
                },
                {
                    "id": "af7b9a40-4a4e-47b3-b0fa-0e2d1e960f6b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "tj_array",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_array",
                    "returnType": 2
                },
                {
                    "id": "2f82ecd4-c760-41a5-86ea-600e5301839a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_is_object",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_is_object",
                    "returnType": 2
                },
                {
                    "id": "e55d45fa-078e-488b-952c-3e7363e2ce97",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_is_array",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_is_array",
                    "returnType": 2
                },
                {
                    "id": "4eca51e3-691a-4c2b-ae28-39af80d606c8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "tj_get",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_get",
                    "returnType": 2
                },
                {
                    "id": "9ef67fdc-5258-436f-af80-b5fc99a4b0c0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        2,
                        2,
                        2
                    ],
                    "externalName": "tj_set",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_set",
                    "returnType": 2
                },
                {
                    "id": "9e400ad6-3034-4d07-aa53-88e7845accf3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_size",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_size",
                    "returnType": 2
                },
                {
                    "id": "f256fd70-81a6-4ee0-b372-c706cc1da428",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_keys",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_keys",
                    "returnType": 2
                },
                {
                    "id": "d697cf36-510c-438e-bb8d-c6fca6de21dd",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_typeof",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_typeof",
                    "returnType": 2
                },
                {
                    "id": "f59b7283-e529-4cbe-893f-6beaf1d1ec00",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_typeid",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_typeid",
                    "returnType": 2
                },
                {
                    "id": "42518bca-711e-45f0-a98e-a75a9a3bc449",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "tj_compare",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_compare",
                    "returnType": 2
                },
                {
                    "id": "2ab74d3c-fa02-4602-8903-1d830791e9f3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "tj_decode",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_decode",
                    "returnType": 2
                },
                {
                    "id": "4d461569-b846-4266-ac9c-97778d261d1a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "tj_encode",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_encode",
                    "returnType": 2
                },
                {
                    "id": "f3a29756-4b21-42fe-8cd8-16e65824bb43",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "tj_init_js",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_init_js",
                    "returnType": 2
                }
            ],
            "init": "tj_init_js",
            "kind": 5,
            "order": [
                
            ],
            "origname": "extensions\\tjson-web.js",
            "uncompress": false
        },
        {
            "id": "3a38db49-23c7-4cfe-a801-7b7a42b69643",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                {
                    "id": "3e5f5f3b-1f1f-4fee-a456-c140c33a4c22",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_true",
                    "hidden": true,
                    "value": "(global.g_tj_true)"
                },
                {
                    "id": "e82e11ea-1a29-44db-b452-8ea1aa98fc9d",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_false",
                    "hidden": true,
                    "value": "(global.g_tj_false)"
                },
                {
                    "id": "dfdf3b2b-e72e-486e-a1d5-5e2f46de8d8f",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_null",
                    "hidden": true,
                    "value": "(global.g_tj_null)"
                },
                {
                    "id": "d37c6f0a-2092-40d0-9869-259e82fde9f5",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_error",
                    "hidden": true,
                    "value": "(global.g_tj_error)"
                },
                {
                    "id": "f3d82f11-1ad1-436c-a0f2-38466b4a0e77",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_undefined",
                    "hidden": true,
                    "value": "0"
                },
                {
                    "id": "0048cb26-1785-4b6f-a173-c6680e1a6c33",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_boolean",
                    "hidden": true,
                    "value": "1"
                },
                {
                    "id": "9b31e7bb-ece4-4425-87b3-f907cbd2213c",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_number",
                    "hidden": true,
                    "value": "2"
                },
                {
                    "id": "6910114e-d293-4452-b0b6-b27e0695bc18",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_string",
                    "hidden": true,
                    "value": "3"
                },
                {
                    "id": "872d369d-f5d6-4350-8ee9-94572bbf9bdb",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_array",
                    "hidden": true,
                    "value": "4"
                },
                {
                    "id": "db5fac1e-9d2b-44bf-99e0-44fe389f4d7b",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_object",
                    "hidden": true,
                    "value": "5"
                },
                {
                    "id": "e87d3cd6-504a-4375-b26c-4515a07e9947",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "tj_type_error",
                    "hidden": true,
                    "value": "6"
                }
            ],
            "copyToTargets": 98336,
            "filename": "tjson-web.gml",
            "final": "",
            "functions": [
                {
                    "id": "dcc44a99-b18a-42b5-bb3c-799229878970",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "tj_init_js_gml",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "tj_init_js_gml",
                    "returnType": 2
                }
            ],
            "init": "tj_init_js_gml",
            "kind": 2,
            "order": [
                
            ],
            "origname": "extensions\\gml.gml",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "cc.yal.tjson",
    "productID": "F79C97955FD1AF4686EB4EF4EF90F0DB",
    "sourcedir": "",
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "1.0.2"
}